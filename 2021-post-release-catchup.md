# Post-release catchup party

Hello again!

Finally, we got all things sorted out that had to be done for us to be able to release 0.47.0. We hope you enjoy it as much as we did making it!

While there have been several huge announcements we've wanted to share with you lately, we decided to wait with these news until after the release of 0.47.0 to avoid stealing too much of the hype. Sorry for that, but we hope you understand. But without further ado, let's dig in!

## Lua is merged

Let's just begin with, arguably, the biggest one. Petr Mikheev worked really hard with this huge commit, and the reviewing from several other devs was fantastic. The result is that the basic framework for OpenMW's future scripting system, Lua, was merged into master a few months ago. This in itself doesn't actually do much for the end user, but modders can already explore its functions. The real power, however, will be unleashed once gameplay mechanics, GUI, graphical features and other exciting things are controllable through Lua commands, what we often refer to as the "de-hardcoding" of OpenMW. Some of this is being worked on right now or has already been merged, like Urm's [basic framework for a Lua-controlled GUI](https://gitlab.com/OpenMW/openmw/-/merge_requests/1138), Mikheev's [Lua-controlled camera](https://gitlab.com/OpenMW/openmw/-/merge_requests/1030) and [raycasting](https://gitlab.com/OpenMW/openmw/-/merge_requests/1175) and Cody Glassman's Lua-supported post-processing shaders. Now, we know you are thinking: "hey wait, post-processing shaders eh? Tell me more! Where's the link?!". We'll discuss this more in a moment, don't worry.

One important clarification must be done before we go on: OpenMW-Lua will *not* be compatible with MWSE-Lua, the 3rd party scripting extender for Morrowind.exe. We aim to make our scripting engine *at least* as powerful as MWSE-Lua, but since MWSE-Lua is a scripting engine for a different game engine, mods made for it will unfortunately *not* be compatible with OpenMW. Current MWSE-Lua mods will therefore have to be rewritten or at least refactored to be able to function in OpenMW. And again, to make it perfectly clear: the implementation of Lua in OpenMW is still a bit immature and will become more powerful as time goes.

We will discuss Lua more later on.

## New ESM-reader

The format that stores the gameplay, quests, scripts and the world in general in Bethesda games like The Elder Scrolls III-V and Fallout 3, NV and 4 is called ESM. The format has been updated every time they release a new game, and OpenMW only supports the format for Morrowind. At the moment at least! 

If you've been paying attention, you might know that a guy with the mysterious nickname cc9cii forked OpenMW way back during 0.36.0, back when we still were using Ogre3D. In this fork, he has been implementing support for Oblivion, Skyrim and Fallout 3/New Vegas with [pretty great results](https://www.youtube.com/user/cc9cii). The fact that he forked during a time where OpenMW still used Ogre3D was unfortunate, however, because that meant his work would need to be more or less completely rewritten for the work to be implemented in OpenMW's master codebase. This is the bad news. The good news is that cc9cii has now joined us and is now doing just that; rewriting his work to implement it into our master codebase. Together with Capo, with his work in the same area, they could hopefully take us many steps in the right direction towards extended support for later Bethesda games.

cc9cii's first really big merge request is a [rewritten ESM-reader](https://gitlab.com/OpenMW/openmw/-/merge_requests/1158), a task he is doing to make it easier to later implement support for later versions of the format. And yes, he already started on that too: [Here](https://gitlab.com/OpenMW/openmw/-/merge_requests/1162) he is working on making OpenMW-CS support the ESM4 (oblivion) format.

## Post-processing shaders

A task needed to bring OpenMW's graphics further into the modern age is the implementation of post-processing shaders. Post-processing shaders are required to implement must-haves like ambient occlusion but also more optional-but-cool details like shallow depth-of-field bokeh effects. 

Our man Cody Glassman has been on fire during 2021 and has been coding one awesome graphical feature after another like there's no tomorrow. [Z-fighting](https://en.wikipedia.org/wiki/Z-fighting) is a [problem no more](https://gitlab.com/OpenMW/openmw/-/merge_requests/889) in OpenMW for example, so silly long view distances seen basically from space will no longer look like garbage due to Z-fighting in the water shorelines like it did previously. But let's return to post-processing shaders: Cody is implementing this [right now](https://gitlab.com/OpenMW/openmw/-/merge_requests/1124) and has already tied it up with Lua, making it possible to make mods that implement visual skooma drunk effects for example but also must-have graphical effects like ambient occlusion. This will be a huge step climbed for OpenMW's graphics capabilties and will be a most welcome addition to our code once it is done. Stay tuned!

## Navmesh disk cache

One exclusive feature OpenMW has that provides great enhancement to our AI is the automatic background generation of navigation meshes. Basically, OpenMW creates a map over the world that tells the AI where the terrain is walkable and where it is not, preventing situations like where NPCs walk straight into walls. The creation of this map is done in runtime with a separate thread, so most modern multi-core computers will not notice any significant loss in performance because of this. For people with weaker computers or mobile devices however, loading times can be significantly longer. Because of this, elsid is working on a feature that will [store the navigation meshes in files](https://gitlab.com/OpenMW/openmw/-/merge_requests/1058) once they have been created, making the generation of them only necessary once.

## Zoomable world map

Ever wanted to be able to zoom out in the world map? See more of the world at the same time? Or less of it, with more detail? Now you can, thanks to Cédric Mocquillon's new feature that allows for [different zoom levels](https://gitlab.com/OpenMW/openmw/-/merge_requests/275) of the map. Simple yet so effective. A very nice addition to the engine.

## Raidrops keep falling on my head

And in the water too. And let's face it, OpenMW's raindrop ripples look pretty terrible. Except they do not anymore, since wareya took the matter in his own hands by making an actual good looking raindrop ripple effect. You can see how it looks in a nightly or on the [merge request](https://gitlab.com/OpenMW/openmw/-/merge_requests/1316). Neat right?

## No more adding mod directories in the openmw.cfg-file

Unless you want to of course. Anyway, Frederic Chardon has had [this merge request](https://gitlab.com/OpenMW/openmw/-/merge_requests/192) up for a very long time but after a lot of discussion regarding the design of it, it seems we will finally see it merged soon. This means you will soon be able to add and remove data directories in a GUI-way directly in the OpenMW launcher. Yay, for most people!

## Magic effects are reworked

EvilEye has been working really hard lately and truckload of MR's merged lately has been his work. One of the bigger projects of his has been the rework of magic effects. For most people, this probably won't be very noticable since it is mostly under-the-hood rework, but quite a few small Morrowind-differences has been solved thanks to this huge task of his. Check out the merge request [here](https://gitlab.com/OpenMW/openmw/-/merge_requests/1116).

That's about it. Or really, it's far from it, but we'll have to round off this blog post somewhere. Thank you so much for sticking around with us through all these years. We'll see you again another time!
