# OpenMW 0.47.0 is now in RC-phase!

Finally, after much hard work to get all the cool new features ready for release, we have our first round of Release Candidates, or "RC" for short. RCs are simply release binaries for testing, to make sure everything is in order for a release. That's right, testing! So we would be *very* greatful if you would download an RC for your OS of choice, test it a bit to see if it works without any issues and report any findings you make to our Gitlab [issue tracker](https://gitlab.com/groups/OpenMW/-/issues). Please make sure to check first that whatever you find is not on the tracker already.

Thank you and we'll see you again on the day of the release!

Downloads are found [here](https://forum.openmw.org/viewtopic.php?p=70914#p70914).
